﻿using Fuji.Configuraciones.DataAccess;
using Fuji.Configuraciones.Extensions;
using Fuji.Configuraciones.Feed2Service;
using System;
using System.Configuration;
using System.IO;
using System.Windows;

namespace Fuji.Configuraciones
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private LoginDataAccess LoginDA;
        private static string UserFeed2 = ConfigurationManager.AppSettings["UserFeed2"] != null ? ConfigurationManager.AppSettings["UserFeed2"].ToString() : "";
        private static string PassFeed2 = ConfigurationManager.AppSettings["PassFeed2"] != null ? ConfigurationManager.AppSettings["PassFeed2"].ToString() : "";
        public static int id_sitio = 0;
        public static Entidades.clsConfiguracion _configura;
        public static string path = ConfigurationManager.AppSettings["ConfigDirectory"] != null ? ConfigurationManager.AppSettings["ConfigDirectory"].ToString() : ""; 
        public static string URL_Feed2 = ConfigurationManager.AppSettings["URL_Feed2"] != null ? ConfigurationManager.AppSettings["URL_Feed2"].ToString() : "";

        public MainWindow()
        {
            try
            {
                InitializeComponent();
                _configura = new Entidades.clsConfiguracion();
                //ConfigDA = new ConfiguracionDataAccess();
                //tbl_ConfigSitio _config = new tbl_ConfigSitio();
                //string mensaje = "";
                if (File.Exists(path + "info.xml"))
                {
                    _configura = XMLConfigurator.getXMLfile();
                    if (_configura != null)
                    {
                        if (_configura.vchClaveSitio != "")
                        {
                            txtSitio.Text = _configura.vchClaveSitio;
                            txtSitio.IsEnabled = false;
                        }
                        else
                        {
                            //    SitioName site = new SitioName();
                            //    site.Show();
                            //    this.Close();
                            txtSitio.IsEnabled = true;
                        }
                    }
                    
                }
                else
                {
                    txtSitio.IsEnabled = true;
                    //    string mensaje = "";
                    //    bool existe = ConfigDA.getVerificaSitio(txtSitio.Text, ref mensaje);
                    //    if (existe)
                    //    {
                    //        tbl_ConfigSitio mdl = new tbl_ConfigSitio();
                    //        ConfigDA.getConfiguracion(txtSitio.Text, ref mensaje);
                    //    }

                }
            }
            catch (Exception eMW)
            {
                MessageBox.Show("Existe un error al iniciar la aplicación: " + eMW.Message);
            }
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtPassword.Password != "" && txtUsuario.Text != "")
                {
                    //verificar en base de datos.
                    //Agregar un superUsuario
                    LoginDA = new LoginDataAccess();
                    string mensaje = "";
                    clsUsuario mdl = new clsUsuario();
                    LoginResponse response = new LoginResponse();
                    //response = LoginDA.Logear(txtUsuario.Text, Security.Encrypt(txtPassword.Password), ref mdl, ref mensaje);
                    response = Login(txtUsuario.Text, Security.Encrypt(txtPassword.Password), ref mdl, ref mensaje);
                    bool successPass = false;
                    bool successUser = false;
                    if (mdl != null)
                    {
                        if ((mdl.vchUsuario != "" && mdl.vchPassword != null) && (mdl.vchUsuario != "" && mdl.vchPassword != ""))
                        {
                            string pass = Security.Decrypt(mdl.vchPassword);
                            successUser = txtUsuario.Text.Trim().ToUpper() == mdl.vchUsuario.Trim().ToUpper();
                            successPass = txtPassword.Password == pass;
                        }
                    }
                    //string pass = Security.Decrypt(_configura.vchPassword);
                    //successUser = txtUsuario.Text.Trim().ToUpper() == _configura.vchUsuario.Trim().ToUpper();
                    //successPass = txtPassword.Password == pass;

                    if (successPass && successUser)
                    {
                        Configuracion conf = new Configuracion(txtSitio.Text, txtUsuario.Text, 1,1);
                        conf.Show();
                        this.Close();
                    }
                    else
                    {
                        mensaje = "";
                        if(!successUser && !successPass)
                        {
                            mensaje = "Usuario y contraseña incorrecta.";
                        }
                        else{
                            if (!successPass)
                            {
                                mensaje = "Contraseña incorrecta.";
                            }
                            if (!successUser)
                            {
                                mensaje = "Usuario incorrecto.";
                            }
                        }
                        MessageBox.Show(mensaje,"Error");
                    }
                }
                else
                {
                    MessageBox.Show("Los campos de usuario y contraseña son requeridos");
                }
            }
            catch (Exception eLogin)
            {
                Log.EscribeLog("Error en el Login: " + eLogin.Message);
                MessageBox.Show("Existe un error, favor de verificar: " + eLogin.Message);
            }
        }

        private LoginResponse Login(string Usuario, string password, ref clsUsuario mdl, ref string mensaje)
        {
            LoginResponse response = new LoginResponse();
            try
            {
                if (_configura != null && _configura.vchUsuario != null && _configura.vchUsuario != "" && _configura.vchPassword != null && _configura.vchPassword != "")
                {
                    response.Success = Usuario == _configura.vchUsuario && password == _configura.vchPassword;
                    mdl.vchPassword = _configura.vchPassword;
                    mdl.vchUsuario = _configura.vchUsuario;
                }
                else
                {
                    mdl.vchPassword = PassFeed2;
                    mdl.vchUsuario = UserFeed2;
                }
            }catch(Exception eLogin)
            {
                response.vchMensaje = eLogin.Message;
                response.Success = false;
                Log.EscribeLog("Existe un error en el Login: " + eLogin.Message);
            }
            return response;
        }

        private void btnSol_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(URL_Feed2);
            }
            catch(Exception eBS)
            {
                Log.EscribeLog("Error al abrir el portar para solicitud de Sitio. " + eBS.Message);
            }
        }
    }
}
